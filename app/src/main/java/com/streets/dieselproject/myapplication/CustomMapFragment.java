package com.streets.dieselproject.myapplication;


import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.streets.dieselprojects.database.GameUsers;
import com.streets.dieselprojects.database.PlayerStatusBean;
import com.streets.dieselprojects.database.PlayersStatus;
import com.streets.network.ServerConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CustomMapFragment extends SupportMapFragment implements
        // map fragment:
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMyLocationButtonClickListener,
        OnMapReadyCallback

{

    private static final String TAG = "CustomMapFragment";


    // Map class variables:
    static final LatLng LOC_TA = new LatLng(32.066158,34.777819);
    public GoogleApiClient mGoogleApiClient;

    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)      // 5s
            .setFastestInterval(16) // 16ms = 6-fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); // most power used, best accuracy (consider PRIORITY_BALANCED_POWER_ACCURACY)

    private GoogleMap mMap;
    private Location currentLocation;
    private boolean firstLocationChange = true;
    LinkedList<Marker> users = new LinkedList<Marker>();
    Marker user; // current device location




    public CustomMapFragment() {
        // Required empty public constructor

        Log.i(TAG,"Inside CustomMapFragment constructor");
        getMapAsync(this);

        Log.i(TAG, "Exiting CustomMapFragment constructor");

    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "inside onConnected");
        LocationServices.FusedLocationApi.requestLocationUpdates
                (mGoogleApiClient,
                        REQUEST,
                        this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "Location changed");

        currentLocation = location;

       if (firstLocationChange == true) {

           mMap.animateCamera(

                   CameraUpdateFactory.newLatLngZoom(
                           new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 16)


           );

           Bitmap faceMarker = Support.getInstance().scale(getResources().getDrawable(R.drawable.menleader));
           user = mMap.addMarker(new MarkerOptions()
                           .position(new LatLng(location.getLatitude(), location.getLongitude()))
                           .icon(BitmapDescriptorFactory.fromBitmap(faceMarker))
           );

           firstLocationChange = false;
       }

       // update marker
       // remove last face marker:
       user.remove();
       // add face markers:

        // TODO: GameUser should be linked to app context
        int iconResId = 0;
        switch (GameUsers.getInstance().currentPlayer){
            case 0:
                iconResId = R.drawable.menleader;
                break;
            case 1:
                iconResId = R.drawable.women1;
                break;
            case 2:
                iconResId = R.drawable.women2;
                break;
        }

       Bitmap faceMarker = Support.getInstance().scale(getResources().getDrawable(iconResId));
       user = mMap.addMarker(new MarkerOptions()
                       .position(new LatLng(location.getLatitude(), location.getLongitude()))
                       .icon(BitmapDescriptorFactory.fromBitmap(faceMarker))
       );


        // update data base location and update other users location:

        // send decision to server:
        ServerConnection serverConnection = ServerConnection.getInstance();

        // create json object
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId",new Integer(GameUsers.getInstance().currentPlayer));
            JSONObject locationObject = new JSONObject();
            locationObject.put("lat", new Double(location.getLatitude()));
            locationObject.put("lng", new Double(location.getLongitude()));
            jsonObject.put("location",locationObject);
            jsonObject.put("message","online");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // encode json object to string
        String jsonEncodeMsg = jsonObject.toString();

        // send server post request
        serverConnection.postSimple(jsonEncodeMsg);

        // add other user locations:
        addPlayersMarker(GameUsers.getInstance().currentPlayer);
    }


    private void addPlayersMarker(int currentPlayer){
        // TODO: modify implementation:
        HashMap<Integer, PlayerStatusBean> playersStatus = PlayersStatus.getInstance().playersStatus;

        // clear all users[] markers:
        for (Marker externalUser: users){
            externalUser.remove();
        }
        users = new LinkedList<Marker>();

        Iterator<Map.Entry<Integer,PlayerStatusBean>> itr = playersStatus.entrySet().iterator();
        while(itr.hasNext()){
            Map.Entry<Integer,PlayerStatusBean> entry = itr.next();
            if (entry.getKey() != currentPlayer){

                LatLng playerLocation = entry.getValue().getLocation();

                // TODO: GameUser should be linked to app context
                int iconResId = 0;
                switch (entry.getKey()){
                case 0:
                    iconResId = R.drawable.menleader;
                    break;
                case 1:
                    iconResId = R.drawable.women1;
                    break;
                case 2:
                    iconResId = R.drawable.women2;
                    break;
                }

                Bitmap faceMarker = Support.getInstance().scale(getResources().getDrawable(iconResId));
                users.add(
                        mMap.addMarker(new MarkerOptions()
                                .position(playerLocation)
                                .icon(BitmapDescriptorFactory.fromBitmap(faceMarker)))
                );
            }
        }


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i(TAG, "Inside map ready");
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMyLocationButtonClickListener(this);

        mMap = googleMap;

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }
}
