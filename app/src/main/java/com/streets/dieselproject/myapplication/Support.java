package com.streets.dieselproject.myapplication;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;

/**
 * Created by idanhahn on 9/9/15.
 */
public class Support {
    private static Support ourInstance = new Support();

    public static Support getInstance() {
        return ourInstance;
    }

    private Support() {
    }

    // example for menleader only
    public Bitmap scale(Drawable drawable){

        //LevelListDrawable d = (LevelListDrawable) drawable;
        //d.setLevel(1234);
        BitmapDrawable bd = (BitmapDrawable) drawable.getCurrent();
        Bitmap b=bd.getBitmap();
        Bitmap newSized = Bitmap.createScaledBitmap(b, b.getWidth()/10,b.getHeight()/10, false);
        return newSized;

    }


    public Bitmap scaleImage(Resources res, int id, int lessSideSize) {
        Bitmap b = null;
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        BitmapFactory.decodeResource(res, id, o);

        float sc = 0.0f;
        int scale = 1;
        // if image height is greater than width
        if (o.outHeight > o.outWidth) {
            sc = o.outHeight / lessSideSize;
            scale = Math.round(sc);
        }
        // if image width is greater than height
        else {
            sc = o.outWidth / lessSideSize;
            scale = Math.round(sc);
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        b = BitmapFactory.decodeResource(res, id, o2);
        return b;
    }


}
