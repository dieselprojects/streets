package com.streets.dieselproject.myapplication;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;


import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends FragmentActivity implements
        // list fragment:
        UserListFragment.OnUserListSelectedListener
{

    private static final String TAG = "Streets Main";
    private boolean isTablet = true;


    // Map class variables:
    static final LatLng LOC_TA = new LatLng(32.066158,34.777819);
    private GoogleApiClient mGoogleApiClient;

    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)      // 5s
            .setFastestInterval(16) // 16ms = 6-fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); // most power used, best accuracy (consider PRIORITY_BALANCED_POWER_ACCURACY)

    private GoogleMap mMap;
    private Location currentLocation;
    private boolean firstLocationChange = true;
    Marker users[];
    Marker user; // current device location




    // Floating action button:
    FloatingActionButton fabPlayers;
    FloatingActionButton fabMap;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // getting screen size:
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        Log.i(TAG,"width " + width + " height " + height);

        if ((width == 1080) || (width == 1794)){

            Log.i(TAG,"TODO - current on handHeld not handling map,list fragment - consider static fragments");
            isTablet = false;


            // players activity:
            fabPlayers = (FloatingActionButton) findViewById(R.id.fab_players);
            fabPlayers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,PlayerListActivity.class);
                    startActivity(intent);
                }
            });



            // Map Activity:
            fabMap = (FloatingActionButton) findViewById(R.id.fab_map);
            fabMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // open map activity:
                    Intent intent = new Intent(MainActivity.this,StreetsMapActivity.class);
                    startActivity(intent);
                }
            });



        } else {


            //SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().
            //        findFragmentById(R.id.fragment_custom_map);
            //if (mapFragment != null)
            //    Log.i(TAG, "Got map fragment");
            //else
            //    Log.e(TAG, "couldn't get mapFragment");

            //mapFragment.getMapAsync(this);


            CustomMapFragment mapFragment = (CustomMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_custom_map);

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(mapFragment)
                    .addOnConnectionFailedListener(mapFragment)
                    .build();

            mapFragment.mGoogleApiClient = mGoogleApiClient;

            // placing bottom right list:
            if (findViewById(R.id.list_fragment) != null) {

                Log.i(TAG, "found list fragment, commiting layout");
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                UserListFragment userListFragment = new UserListFragment();
                fragmentTransaction.add(R.id.list_fragment, userListFragment);
                fragmentTransaction.commit();
                Log.i(TAG, "done commiting layout");


            } else {
                Log.e(TAG, "could not find list fragment");
            }

        }

        final ImageView imageViewRiddle2 = (ImageView) findViewById(R.id.image_view_riddle2);
        imageViewRiddle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewRiddle2.setImageResource(R.drawable.meir_dizengoff2);
            }
        });

        final ImageView imageViewRiddle3 = (ImageView) findViewById(R.id.image_view_riddle3);
        imageViewRiddle3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageViewRiddle3.setImageResource(R.drawable.meir_dizengoff3);
            }
        });





    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserSelected(int position) {
        Log.i(TAG, "user selected " + position);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "Inside onResume");
        if (isTablet)
            mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "Inside onPause");
        if (isTablet)
            mGoogleApiClient.disconnect();
    }

    @Override
    protected void onStart() {
        super.onStart();
        DialogFragment choosePlayerDialog = new ChoosePlayerDialog();
        choosePlayerDialog.show(getFragmentManager(),"choosePlayers");
    }
}
