package com.streets.dieselproject.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import com.streets.dieselprojects.database.GameUsers;
import com.streets.network.ServerConnection;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by idanhahn on 9/11/15.
 */
public class ChoosePlayerDialog extends DialogFragment{

    private static final String TAG="ChoosePlayerDialog";


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.choose_user).setItems(R.array.players_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GameUsers.getInstance().currentPlayer = which;
                Log.i(TAG, "player choose " + which);

                // send decision to server:
                ServerConnection serverConnection = ServerConnection.getInstance();

                // create json object
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userId",new Integer(which));
                    JSONObject locationObject = new JSONObject();
                        locationObject.put("lat", new Double(0.0));
                        locationObject.put("lng", new Double(0.0));
                    jsonObject.put("location",locationObject);
                    jsonObject.put("message","online");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // encode json object to string
                String jsonEncodeMsg = jsonObject.toString();

                // send server post request
                serverConnection.postSimple(jsonEncodeMsg);

            }
        });
        return builder.create();
    }
}
