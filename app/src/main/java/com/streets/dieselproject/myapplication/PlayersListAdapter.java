package com.streets.dieselproject.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.streets.dieselproject.myapplication.R;
import com.streets.dieselprojects.database.UserBean;

import java.util.ArrayList;

/**
 * Created by idanhahn on 9/8/15.
 */
public class PlayersListAdapter extends ArrayAdapter<UserBean>{

    private final Context context;
    private final ArrayList<UserBean> users;


    public PlayersListAdapter(Context context,ArrayList<UserBean> users){
        super(context, R.layout.list_entry,users );

        this.context = context;
        this.users = users;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_entry, parent, false);

        ImageView playerPic = (ImageView) rowView.findViewById(R.id.list_image_view_face);
        TextView playerName = (TextView) rowView.findViewById(R.id.list_text_view_name);

        int imageId = context.getResources().getIdentifier("drawable/" + users.get(position).getIcon(),null,context.getPackageName());

        playerPic.setImageResource(imageId);
        playerName.setText(users.get(position).getName());

        return rowView;
    }
}
