package com.streets.dieselproject.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.streets.dieselprojects.database.GameUsers;
import com.streets.dieselprojects.database.UserBean;

/**
 * Created by idanhahn on 9/6/15.
 */
public class UserListFragment extends ListFragment{

    // Communication interface here:
    OnUserListSelectedListener mCallback;

    public interface OnUserListSelectedListener{
        public void onUserSelected(int position);
    }


        GameUsers gameUsers = GameUsers.getInstance();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int layout = android.R.layout.simple_list_item_1;
        setListAdapter(new PlayersListAdapter(getActivity(), gameUsers.users));

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof OnUserListSelectedListener){

            mCallback = (OnUserListSelectedListener) activity;

        } else{

            throw new ClassCastException(activity.toString() + " must implement onUserListSelectedListener");

        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        // this is the main activity handle ( right now mainActivity onUserSelect function will fire)
        mCallback.onUserSelected(position);

        getListView().setItemChecked(position,true);

    }
}
