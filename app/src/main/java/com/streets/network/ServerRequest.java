package com.streets.network;

import android.os.AsyncTask;
import android.util.Log;

import com.streets.dieselprojects.database.PlayerStatusBean;
import com.streets.dieselprojects.database.PlayersStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;


/**
 * Created by idanhahn on 9/11/15.
 */
public class ServerRequest extends AsyncTask<String,Integer,String>{

    private static final String TAG = "ServerRequest";

    private String requestMsg;

    public ServerRequest(String requestMsg){
        this.requestMsg = requestMsg;
    }


    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection connection = null;
        // send Server request
        String serverLocation = "http://streets-server-dieselprojects.c9.io/";
        String response = "";
        try {
            URL url = new URL(serverLocation);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();

            // sending json:
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(requestMsg);
            out.close();

            int responseCode = connection.getResponseCode();
            Log.d(TAG, "The response is: " + responseCode);

            InputStream is = connection.getInputStream();
            response = readQuick(is);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.disconnect();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        // response arrives here
        Log.i(TAG,"Response " + s);
        try {
            Log.i(TAG,s);
            JSONArray responseArray = new JSONArray(s);

            for (int i = 0 ; i < responseArray.length(); i++){
                JSONObject entry = (JSONObject) responseArray.get(i);

                // update players status:
                PlayerStatusBean playerStatus = new PlayerStatusBean(entry);
                PlayersStatus.getInstance().setPlayerStatus(playerStatus.getPlayerId(),playerStatus);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    // Reads an InputStream and converts it to a String.
    private String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    private String readQuick(InputStream stream) throws IOException{

        InputStreamReader isr = new InputStreamReader(stream);
        BufferedReader br = new BufferedReader(isr);

        StringBuilder readResult = new StringBuilder();
        String line;
        while((line = br.readLine())!=null){
            readResult.append(line);
        }
        return readResult.toString();
    }

}
