package com.streets.network;

import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by idanhahn on 9/11/15.
 */
public class ServerConnection {
    private static ServerConnection ourInstance = new ServerConnection();

    public static ServerConnection getInstance() {
        return ourInstance;
    }

    private ServerConnection() {
    }


    public void postSimple(String msg){

        ServerRequest request = new ServerRequest(msg);
        request.execute();

    }

    private void callAsynchronousTask(String msg) {
        final Handler handler = new Handler();

        Timer timer = new Timer();

        TimerTask serverReq = new TimerTask(){

            @Override
            public void run() {
                handler.post(new Runnable(){

                    @Override
                    public void run() {

                        try{
                            ServerRequest request = new ServerRequest("Dont know yet");
                            request.execute();
                        } catch (Exception e){
                            System.exit(1);
                        }

                    }
                });
            }
        };
        timer.schedule(serverReq,0,5000);
    }
}
