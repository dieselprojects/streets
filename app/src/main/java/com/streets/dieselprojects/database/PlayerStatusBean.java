package com.streets.dieselprojects.database;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by idanhahn on 9/12/15.
 */
public class PlayerStatusBean{

    private Integer playerId;
    private LatLng location;
    private String message;

    public PlayerStatusBean(Integer playerId, LatLng location, String message) {
        this.playerId = playerId;
        this.location = location;
        this.message = message;
    }

    public PlayerStatusBean(JSONObject status){
        try {
            this.playerId = status.getInt("userId");
            JSONObject location = status.getJSONObject("location");
            this.location = new LatLng(location.getDouble("lat"),location.getDouble("lng"));
            this.message = status.getString("msg");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
