package com.streets.dieselprojects.database;

import java.util.ArrayList;

/**
 * Created by idanhahn on 9/6/15.
 */
public class GameUsers {

    private static GameUsers ourInstance = new GameUsers();

    public static GameUsers getInstance() {
        return ourInstance;
    }

    // TODO: simple implementation to choose user from predefine options
    // 1 Adam, 2 Karen, 3 Emma
    public int currentPlayer;

    public ArrayList<UserBean> users;

    private GameUsers() {
        // TODO: currentlly only support static users:
        UserBean adam = new UserBean("Adam","menleader");
        UserBean karen = new UserBean("Karen","women1");
        UserBean emma = new UserBean("Emma","women2");
        UserBean aaron = new UserBean("Aaron","men1");
        UserBean daniel = new UserBean("Daniel","men2");

        users = new ArrayList<UserBean>();
        users.add(adam);
        users.add(karen);
        users.add(emma);
        users.add(aaron);
        users.add(daniel);
    }


    public String[] getNames(){
        String[] icons = new String[3];
        for(int i = 0; i < users.size(); i++){
            icons[i] = users.get(i).getName();
        }
        return icons;
    }

    public String[] getIcons(){
        String[] icons = new String[3];
        for(int i = 0; i < users.size(); i++){
            icons[i] = users.get(i).getIcon();
        }
        return icons;
    }
}
