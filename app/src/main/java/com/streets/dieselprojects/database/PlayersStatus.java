package com.streets.dieselprojects.database;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by idanhahn on 9/12/15.
 */
public class PlayersStatus {
    private static PlayersStatus ourInstance = new PlayersStatus();

    public static PlayersStatus getInstance() {
        return ourInstance;
    }

    private PlayersStatus() {
        playersStatus = new HashMap<Integer,PlayerStatusBean>();
    }

    public HashMap<Integer,PlayerStatusBean> playersStatus;

    public PlayerStatusBean getPlayerStatus(Integer key){
        return playersStatus.get(key);
    }

    public void setPlayerStatus(Integer key, PlayerStatusBean status){
        playersStatus.put(key,status);
    }

}
