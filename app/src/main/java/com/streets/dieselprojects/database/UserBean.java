package com.streets.dieselprojects.database;

/**
 * Created by idanhahn on 9/6/15.
 */
public class UserBean {

    private String name;
    private String icon;


    public UserBean(String name, String icon) {
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }



}
